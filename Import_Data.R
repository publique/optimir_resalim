####-----Directory-------####
IMP <- "optimir_resalim/IMPORT"
EXP <- "optimir_resalim/EXPORT"

####-----Library-------####
library(data.table)

####-----Import fichier découpé-------####
setwd(IMP)

files = list.files(pattern="*.csv")
data_list = lapply(files, fread,select = c(1:5),sep = ";")#import via fread en séléction de colones
dt1<- do.call(rbind, data_list)
dt1<-as.data.table(dt1)

files = list.files(pattern="*.csv")
data_list = lapply(files, fread,select = c(1,6:10),sep = ";")
dt2<- do.call(rbind, data_list)
dt2<-as.data.table(dt2)

files = list.files(pattern="*.csv")
data_list = lapply(files, fread,select = c(1,11:15),sep = ";")
dt3<- do.call(rbind, data_list)
dt3<-as.data.table(dt3)

####-----Croisement des données-------####
data <- cbind(dt1,dt2)
data <- cbind(data,dt3)

####-----Import fichier découpé-------####
setwd(EXP)
saveRDS(data, file = "data.rds")
